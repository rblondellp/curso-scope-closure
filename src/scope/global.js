//variables

var a; //declarando
var b = 'b'; //declaramos y asignamos
b = 'bb'; // reasignacion
var a = 'aa'; // redeclaracion

//Global Scope
var fruit = 'Apple'; //global

function bestFruit() {
    console.log(fruit);
}

bestFruit(fruit);

function countries() {
    country = 'Colombia'; //declaracion global
    console.log(country);
}
countries();
console.log(country);

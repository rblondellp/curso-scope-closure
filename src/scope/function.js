function greeting() {
    let userName = 'Ray';
    console.log(userName);

    if (userName === 'Ray') {
        console.log(`Hello ${userName}`);
    }
}

greeting();
console.log(userName);

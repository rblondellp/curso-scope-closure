var firstName; //Undefined
firstName = 'Ray';
console.log(firstName);

var lastName = 'David'; //declarar /asignar
lastName = 'Ana'; //reasignar
console.log(lastName);

var secondName = 'Andersson'; //declarando y asignando
var secondName = 'Ana'; //reasignado
console.log(secondName);

// let
let fruit = 'Apple'; //declarar y asignar
fruit = 'Kiwi'; //reasginar
console.log(fruit);
// let fruit = 'Banana';

// const
const animal = 'dog'; //declare y asigna
// animal = 'cat'; //reasginando
console.log(animal);

const vehicles = [];
vehicles.push('car');
console.log(vehicles);

vehicles.pop();
console.log(vehicles);
